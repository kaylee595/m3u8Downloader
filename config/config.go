package config

import (
	"github.com/kaylee595/tracerr"
	"github.com/spf13/viper"
	"os"
	"path/filepath"
	"runtime"
)

type Config struct {
	Concurrency       uint   // Concurrency 并发数量
	FfmpegBinFilename string // FfmpegBinFilename ffmpeg文件路径
	RetryOfNum        uint   // RetryOfNum 下载失败重试次数, 默认0, 不进行重试
	AllowErrorOfNum   uint   // AllowErrorOfNum 允许出错数, 默认0, 出现错误将终止下载

	ProxyAddr             string // ProxyAddr 代理地址(http://localhost:10809)
	DownloadTimeoutSecond int    // DownloadTimeoutSecond 下载超时(秒), 默认50, 每个TS下载时间不得超过50秒
	TsMinKB               uint   // TsMinKB 下载的TS最小体积, 如果小于设定的值表示下载失败, 默认0, 不对TS大小进行限制

	RefererConfigs []RefererConfig // RefererConfigs 发起请求时为请求添加referer, 将根据配置自动选择对应的Referer
	UserAgent      string          // UserAgent 发起请求时为请求添加UserAgent
}

type RefererConfig struct {
	Rule    string // Rule 匹配规则, 正则表达式匹配.
	Referer string // Referer 相对应的Referer
}

var Default = CorrectionConfig(&Config{})

// CorrectionConfig 为Config添加一些默认值
func CorrectionConfig(cfg *Config) *Config {
	if cfg.Concurrency == 0 {
		cfg.Concurrency = uint(runtime.NumCPU())
	}
	if cfg.DownloadTimeoutSecond <= 0 {
		cfg.DownloadTimeoutSecond = 50
	}
	return cfg
}

func LoadConfig(filename ...string) (*Config, error) {
	var configFile string
	if len(filename) > 0 {
		configFile = filename[0]
	}
	if len(configFile) > 0 {
		viper.SetConfigFile(configFile)
	} else {
		viper.SetConfigType("yaml")
		viper.SetConfigName("config")

		viper.AddConfigPath(".") // 优先当前工作目录查找配置文件
		executable, err := os.Executable()
		if err == nil {
			executable = filepath.Dir(executable)
			viper.AddConfigPath(executable) // 其次在软件运行目录下查找文件
		}
	}
	err := viper.ReadInConfig()
	if err != nil {
		return nil, tracerr.Wrap(err)
	}
	ret := new(Config)
	err = viper.Unmarshal(ret)
	if err != nil {
		return nil, tracerr.Wrap(err)
	}
	return CorrectionConfig(ret), nil
}
