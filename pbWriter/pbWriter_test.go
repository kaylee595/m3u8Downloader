package pbWriter

import (
	"fmt"
	"os"
	"sync"
	"testing"
	"time"
)

func TestProgressOutput_Write(t *testing.T) {
	writer := NewProgressOutput(os.Stderr)
	var wg sync.WaitGroup
	wg.Add(4)
	go func() {
		defer wg.Done()
		for i := range 20 {
			fmt.Fprintf(writer, "\rnum:(%d)", i)
			time.Sleep(time.Millisecond * 200)
		}
	}()
	go func() {
		defer wg.Done()
		for range 10 {
			fmt.Fprintln(writer, "这是一条日志")
			time.Sleep(time.Millisecond * 200)
		}
	}()
	go func() {
		defer wg.Done()
		for range 10 {
			fmt.Fprintln(writer, "这是一条日志")
			time.Sleep(time.Millisecond * 250)
		}
	}()
	go func() {
		defer wg.Done()
		for range 10 {
			fmt.Fprintln(writer, "这是一条日志")
			time.Sleep(time.Millisecond * 100)
		}
	}()
	wg.Wait()
}
