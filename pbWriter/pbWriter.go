package pbWriter

import (
	"github.com/kaylee595/tracerr"
	"io"
	"sync"
)

type ProgressOutput struct {
	w           io.Writer
	mu          sync.Mutex
	lastBarData []byte
}

func NewProgressOutput(w io.Writer) *ProgressOutput {
	return &ProgressOutput{w: w}
}

func (w *ProgressOutput) Write(p []byte) (int, error) {
	w.mu.Lock()
	defer w.mu.Unlock()

	if p[0] != '\r' {
		// 这不是一条进度条数据, 清空当前行数据
		_, err := w.w.Write([]byte{'\r'})
		if err != nil {
			return 0, tracerr.Wrap(err)
		}
		// 写出最新数据
		n, err := w.w.Write(p)
		if err != nil {
			return n, tracerr.Wrap(err)
		}
		// 还原进度条数据
		if len(w.lastBarData) > 0 && w.lastBarData[0] == '\r' && w.lastBarData[len(w.lastBarData)-1] != '\n' {
			_, err = w.w.Write(w.lastBarData)
			if err != nil {
				return 0, tracerr.Wrap(err)
			}
		}
		return n, nil
	}

	n, err := w.w.Write(p)
	if err != nil {
		return n, tracerr.Wrap(err)
	}
	w.lastBarData = p
	return n, nil
}
