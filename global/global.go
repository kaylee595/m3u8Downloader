package global

import (
	"m3u8Downloader/pbWriter"
	"os"
)

var StdErr = pbWriter.NewProgressOutput(os.Stderr)
