package core

type DownloadMediaEvent interface {
	BeforeDownloadMedia(ctx *Context) error
	// AfterDownloadMedia 这里返回错误应当终止全部Media的下载
	AfterDownloadMedia(ctx *Context) error
}
