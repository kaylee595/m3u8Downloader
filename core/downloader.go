package core

import (
	"context"
	"gitee.com/kaylee595/gkaylee"
	"gitee.com/kaylee595/gretry"
	"gitee.com/kaylee595/m3u8"
	"github.com/kaylee595/tracerr"
	"io"
	"net/http"
	"sync"
)

type Downloader struct {
	httpClient         *http.Client
	concurrentMax      int
	downloadRetriesMax int
	keyCache           map[string][]byte
	getKeyRWMutex      sync.RWMutex
}

func NewDownloader(options ...Option) *Downloader {
	ret := &Downloader{
		httpClient:         http.DefaultClient,
		concurrentMax:      3,
		downloadRetriesMax: 3,
		keyCache:           make(map[string][]byte, 1),
	}
	for _, option := range options {
		option(ret)
	}
	return ret
}

type Option func(*Downloader)

func WithConcurrentMax(val int) Option {
	if val <= 0 {
		val = 1
	}
	return func(d *Downloader) {
		d.concurrentMax = val
	}
}

func WithDownloadRetriesMax(val int) Option {
	return func(d *Downloader) {
		d.downloadRetriesMax = val
	}
}

func WithHTTPClient(val *http.Client) Option {
	if val == nil {
		val = http.DefaultClient
	}
	return func(d *Downloader) {
		d.httpClient = val
	}
}

func (d *Downloader) Download(ctx context.Context, playList []*m3u8.Media, event DownloadMediaEvent) error {
	ctx, cancelFunc := context.WithCancelCause(ctx)
	defer cancelFunc(nil)

	taskChan := make(chan *m3u8.Media, 16)

	// worker
	wg := sync.WaitGroup{}
	for range d.concurrentMax {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for task := range taskChan {
				select {
				case <-ctx.Done():
					// 继续消费task, 但不进行任何操作
					continue
				default:
				}

				// 资源获取前回调
				err := d.beforeDownloadMedia(event, NewContext(ctx, task, nil))
				if err != nil {
					err = d.afterDownloadMedia(event, NewContext(WithCanceledContext(ctx, err), task, nil))
					if err != nil {
						cancelFunc(err)
					}
					continue
				}

				// 获取资源
				payload, err := tracerr.Wrap1(d.getResponsePayload(ctx, task.Addr))
				if err == nil {
					if task.Key != nil {
						var key []byte
						key, err = tracerr.Wrap1(d.getKey(ctx, task.Key.URI))
						if err == nil {
							_, err = tracerr.Wrap1(task.Key.Decrypt(key, payload, payload))
						}
					}
				}

				// 资源获取后回调
				select {
				case <-ctx.Done():
					// 上下文被取消则无需回调
					continue
				default:
				}
				var afterCallbackCtx *Context
				if err != nil {
					afterCallbackCtx = NewContext(WithCanceledContext(ctx, err), task, payload)
				} else {
					afterCallbackCtx = NewContext(ctx, task, payload)
				}
				err = d.afterDownloadMedia(event, afterCallbackCtx)
				if err != nil {
					cancelFunc(err)
					continue
				}

			}
		}()
	}

	// dispatch
	for _, media := range playList {
		taskChan <- media
	}
	close(taskChan)

	wg.Wait()
	return context.Cause(ctx)
}

func (d *Downloader) beforeDownloadMedia(event DownloadMediaEvent, ctx *Context) error {
	if event != nil {
		return event.BeforeDownloadMedia(ctx)
	}
	return nil
}

func (d *Downloader) afterDownloadMedia(event DownloadMediaEvent, ctx *Context) error {
	if event != nil {
		return event.AfterDownloadMedia(ctx)
	}
	return nil
}

func (d *Downloader) getResponsePayload(ctx context.Context, addr string) ([]byte, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, addr, nil)
	if err != nil {
		return nil, tracerr.Wrap(err)
	}
	// 获取资源数据
	data, err := gretry.Do[[]byte](func() (any, error) {
		resp, err := d.httpClient.Do(req)
		if err != nil {
			return nil, tracerr.Wrap(err)
		}
		defer func() {
			gkaylee.IgnoreErr0(resp.Body.Close())
		}()
		data := make([]byte, resp.ContentLength)
		_, err = io.ReadFull(resp.Body, data)
		if err != nil {
			return nil, tracerr.Wrap(err)
		}
		return data, nil
	})
	if err != nil {
		return nil, tracerr.Wrap(err)
	}
	return data, nil
}

func (d *Downloader) getKey(ctx context.Context, addr string) ([]byte, error) {
	// 并发读
	d.getKeyRWMutex.RLock()
	key, ok := d.keyCache[addr]
	if ok {
		d.getKeyRWMutex.RUnlock()
		return key, nil
	}
	d.getKeyRWMutex.RUnlock()
	// 同步操作
	d.getKeyRWMutex.Lock()
	defer d.getKeyRWMutex.Unlock()
	// 二次检查
	key, ok = d.keyCache[addr]
	if ok {
		return key, nil
	}

	// 请求数据
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, addr, nil)
	if err != nil {
		return nil, tracerr.Wrap(err)
	}
	resp, err := d.httpClient.Do(req)
	if err != nil {
		return nil, tracerr.Wrap(err)
	}
	defer func() {
		gkaylee.IgnoreErr0(resp.Body.Close())
	}()
	if resp.StatusCode != http.StatusOK {
		return nil, tracerr.Errorf("http status code not 200, code: %d", resp.StatusCode)
	}
	key = make([]byte, resp.ContentLength)
	_, err = io.ReadFull(resp.Body, key)
	if err != nil {
		return nil, tracerr.Wrap(err)
	}
	d.keyCache[addr] = key
	return key, nil
}

func WithCanceledContext(parent context.Context, err error) context.Context {
	ctx, cancel := context.WithCancelCause(parent)
	cancel(err)
	return ctx
}
