package core

import (
	"context"
	"gitee.com/kaylee595/m3u8"
)

type Context struct {
	context.Context
	Media           *m3u8.Media
	ResponsePayload []byte
}

func NewContext(context context.Context, media *m3u8.Media, responsePayload []byte) *Context {
	return &Context{
		Context:         context,
		Media:           media,
		ResponsePayload: responsePayload,
	}
}
