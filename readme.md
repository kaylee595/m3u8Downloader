### 介绍
m3u8Downloader是用来下载m3u8视频的, 轻轻松松跑满全部带宽来进行下载~
1. 支持并发下载媒体流
2. 支持加密的媒体资源
3. 媒体资源的合并
4. 媒体资源转MP4. 

相比[m3u8Download](https://gitee.com/kaylee595/m3u8-download):
1. 整体进行重构, 将M3U8文件的解析逻辑抽离到模块([感兴趣可以在这里查看, 欢迎改进](https://gitee.com/kaylee595/m3u8))
2. 下载性能更加优越, 合理使用多路复用减少请求TCP创建的时间.
### 使用
在开始下载TS之前, 我们需要将M3U8文件下载到本地, 然后再去下载TS流, 这是两个步骤. 你可以自己手动下载好m3u8文件, 也可以使用软件自带的m3u8下载器来下载m3u8文件. 注意, 自己下载的m3u8文件务必确保m3u8文件中的资源链接是绝对地址.
#### 下载M3U8文件
> m3u8文件下载, 会自动转换m3u8数据中的相对路径为绝对路径.<br>
> 
> Usage:<br>
>   m3u8Downloader m3u8 {m3u8URL地址} [flags]<br>
>
> Flags:<br>
>   -h, --help&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;help for m3u8<br>
>   -o, --output string&nbsp;&nbsp;&nbsp;输出的文件名(Default: 链接尾部的名称)
```shell
# Example
# 这将下载链接中的数据到"周处除三害.m3u8"
.\m3u8Downloader.exe m3u8 https://example.com/100.m3u8 -o "周处除三害"
```
#### 下载M3U8视频
> 将m3u8中的所有媒体流全部下载到本地后合并成视频
> 
> Usage:<br>
> m3u8Downloader {m3u8文件} [flags]<br>
> 
> Flags:<br>
> --config string&nbsp;&nbsp;&nbsp;config file (default is currentDirectory/config.yaml)<br>
> -h, --help&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;help for m3u8Downloader<br>
```shell
# Example
# 使用之前, 确保你已经在配置文件中配置好了ffmpeg路径, 如果没有, 将不会自动把下载好的媒体转为mp4.
# 这将下载"周处除三害.m3u8"中的媒体到当前目录下的media文件夹
# 全部媒体下载完后会自动合并成一个, 最后会调用ffmpeg将合并的媒体流转为mp4存储到mp4文件夹.
.\m3u8Downloader.exe "周处除三害.m3u8"
```
#### 配置文件
在当前目录下创建一个`config.yaml`文件, 配置以下内容.
```yaml
concurrency: 16 # 并发数量, 默认为CPU核心数量
ffmpegBinFilename: "" # ffmpeg文件路径, 为空时, 将不自动将TS合并后的视频转为MP4
retryOfNum: 3 # 下载失败重试次数, 默认0, 不进行重试
allowErrorOfNum: 60 # 允许出错数, 默认0, 出现错误将终止下载

proxyAddr: "" # 代理地址, 为空不使用代理, 格式参考:http://localhost:10809
downloadTimeoutSecond: 0 # 下载超时(秒), lte 0时, 该值等同于50, 该值默认即为50.
tsMinKB: 20 # 下载的TS最小体积, 如果小于设定的值表示下载失败, 默认0, 不对TS大小进行限制

# refererConfigs 发起请求时为请求添加Referer协议头, 将根据配置自动选择对应的referer地址, 其中rule是正则表达式
refererConfigs:
  - rule: ^www\.example\.com$
    referer: https://www.example.com/
# userAgent 发起请求时为请求添加UserAgent协议头, 为空时, 不添加 User-Agent 协议头
userAgent: ""
```
#### 清理视频
当你成功下载到MP4视频之后, 你会发现media目录下面的文件是多余的, 可以清理掉, 但是有时候手动清理太多视频比较麻烦, 因为你有可能需要一个个对比看看media中的文件夹中哪个是已经下载成功有MP4文件了, 可以借助这个命令来快速安全的删除.
```shell
.\m3u8Downloader.exe clean
```
执行命令后, 程序会扫描`mp4`文件夹下面下载成功的视频, 将扫描到的视频文件名对应的在`media`文件夹中进行匹配, 如果匹配到了, 程序会自动删除那些没用的媒体文件. 该命令只会删除`media`文件夹下的文件, 不会删除其他目录的文件, 可以放心大胆的执行!
### 联系我
在使用过程中遇到问题, 欢迎随时提问我, 你可以在[Issues](https://gitee.com/kaylee595/m3u8Downloader/issues)中留下你的问题或建议.<br>
如果你有能力解决本仓库的BUG, 欢迎你在[Pull Request](https://gitee.com/kaylee595/m3u8Downloader/pulls)中提交你的改进.