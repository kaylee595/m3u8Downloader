/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"gitee.com/kaylee595/gkaylee"
	"io/fs"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
)

// cleanCmd represents the clean command
var cleanCmd = &cobra.Command{
	Use:   "clean",
	Short: "清理冗余文件",
	Long:  `扫描当前目录下mp4文件夹中的视频, 将转换成功的视频对应的TS文件进行删除, 只保留mp4的文件.`,
	Run: func(cmd *cobra.Command, args []string) {
		const (
			ExtMP4 = ".mp4"
		)
		fn := func(path string, d fs.DirEntry, err error) error {
			gkaylee.PanicIfError0(err)
			if d.IsDir() || filepath.Ext(path) != ExtMP4 {
				return nil
			}
			projectName := gkaylee.GetFilename(d.Name())
			if name := filepath.Join(tsSaveDir, projectName); gkaylee.PanicIfError(gkaylee.CheckFileExist(name)) {
				_ = os.RemoveAll(name)
			}
			if name := filepath.Join(tsSaveDir, projectName+".ts"); gkaylee.PanicIfError(gkaylee.CheckFileExist(name)) {
				_ = os.Remove(name)
			}
			return nil
		}
		output, _ := cmd.Flags().GetString("output")
		if len(output) > 0 {
			lstat, err := os.Lstat(output)
			if err != nil {
				_ = fn(output, nil, err)
				return
			}
			_ = fn(output, fs.FileInfoToDirEntry(lstat), nil)
			return
		}
		gkaylee.PanicIfError0(filepath.WalkDir(mp4SaveDir, fn))
	},
}

func init() {
	rootCmd.AddCommand(cleanCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// cleanCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// cleanCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
