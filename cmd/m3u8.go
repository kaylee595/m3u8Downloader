/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"gitee.com/kaylee595/gkaylee"
	"gitee.com/kaylee595/m3u8"
	"github.com/spf13/cobra"
	"log/slog"
	"net/http"
	"net/url"
	"os"
	"path"
	"regexp"
	"strings"
)

// m3u8Cmd represents the m3u8 command
var m3u8Cmd = &cobra.Command{
	Use:   "m3u8 {m3u8URL地址}",
	Short: "m3u8文件下载",
	Long:  `m3u8文件下载, 会自动转换m3u8数据中的相对路径为绝对路径.`,
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		addr := args[0]
		output, _ := cmd.Flags().GetString("output")

		// logger
		log := slog.With(slog.String("addr", addr))

		var success bool
		defer func() {
			if !success {
				os.Exit(1)
			}
		}()

		// 参数校验
		if !regexp.MustCompile(`https?://`).MatchString(addr) {
			log.Error("请传入http或https的链接")
			return
		}
		if len(output) == 0 {
			addrParse, err := url.Parse(addr)
			if err != nil {
				log.Error("地址解析失败", slog.Any("err", err))
				return
			}
			output = path.Base(addrParse.Path)
			if len(output) == 0 {
				log.Error("无法为输出文件起名, 请使用-o参数指定文件名")
				return
			}
		}
		if !strings.HasSuffix(output, ".m3u8") && !strings.HasSuffix(output, ".m3u") {
			output += ".m3u8"
		}
		// 从网络加载m3u8文件
		req, err := http.NewRequest(http.MethodGet, addr, nil)
		if err != nil {
			log.Error("创建请求失败", slog.Any("err", err))
			return
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			log.Error("请求M3U8链接失败", slog.Any("err", err))
			return
		}
		defer func() {
			gkaylee.IgnoreErr0(resp.Body.Close())
		}()
		m8, err := m3u8.ParseM3U8FromReader(resp.Body, req.URL.JoinPath(".."))
		if err != nil {
			log.Error("m3u8数据解析失败", slog.Any("err", err))
			return
		}
		err = os.WriteFile(output, []byte(m8.String()), 0600)
		if err != nil {
			log.Error("m3u8数据保存到文件失败", slog.Any("err", err))
			return
		}
		log.Info("m3u8文件下载成功", "filename", output)
		success = true
	},
}

func init() {
	rootCmd.AddCommand(m3u8Cmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	m3u8Cmd.PersistentFlags().StringP("output", "o", "", "输出的文件名(Default: 链接尾部的名称)")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// m3u8Cmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
