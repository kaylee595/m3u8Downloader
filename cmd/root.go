package cmd

import (
	"bufio"
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"gitee.com/kaylee595/gkaylee"
	"gitee.com/kaylee595/go-ffmpeg"
	"gitee.com/kaylee595/gretry"
	"gitee.com/kaylee595/gretry/retryTransport"
	"gitee.com/kaylee595/m3u8"
	"github.com/cheggaaa/pb/v3"
	"github.com/hashicorp/go-multierror"
	"github.com/kaylee595/tracerr"
	"github.com/lmittmann/tint"
	"github.com/samber/lo"
	"github.com/spf13/viper"
	"log/slog"
	"m3u8Downloader/config"
	"m3u8Downloader/core"
	"m3u8Downloader/downloadMediaEventBus"
	"m3u8Downloader/global"
	"m3u8Downloader/roundTriper"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"github.com/spf13/cobra"
)

const (
	tsSaveDir  = "media"
	mp4SaveDir = "mp4"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "m3u8Downloader {m3u8文件}",
	Short: "m3u8视频下载",
	Long:  `将m3u8中的所有媒体流全部下载到本地后合并成视频`,
	Args:  cobra.MinimumNArgs(1),
	// Run 程序入口
	Run: func(cmd *cobra.Command, args []string) {
		m3u8Filename := args[0]
		logger := slog.With(slog.String("m3u8Filename", m3u8Filename))

		var success bool
		defer func() {
			if !success {
				os.Exit(1)
			}
		}()

		// 检查m3u8文件是否下载过
		exist, err := gkaylee.CheckFileExist(m3u8Filename + ".ok")
		if err != nil {
			logger.Error("检查文件是否存在出错",
				slog.Any("err", err),
			)
			return
		}
		if exist {
			logger.Warn("该文件已有下载成功的m3u8")
			return
		}

		// 优雅退出程序
		ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
		defer stop()

		projectName := gkaylee.GetFilename(m3u8Filename)
		tsSavePath := filepath.Join(tsSaveDir, projectName)
		cfg := config.Default

		// 日志
		logger = slog.With(slog.String("projectName", projectName))
		// 加载m3u8文件
		m8, err := m3u8.ParseM3U8FromFile(m3u8Filename, nil)
		if err != nil {
			logger.Error("解析M3U8文件失败",
				slog.Any("err", err),
			)
			return
		}
		// 进度条
		bar := newProgressBar(len(m8.PlayList), lo.Substring(projectName, 0, 20))
		// 设置进度条初始位置
		downloadedOfNum := lo.Reduce(m8.PlayList, func(agg int64, media *m3u8.Media, index int) int64 {
			urlObj, err := url.Parse(media.Addr)
			if err != nil {
				slog.Warn("链接解析失败", slog.Any("err", err))
				return agg
			}
			filename := filepath.Join(tsSavePath, filepath.Base(urlObj.Path))
			if !strings.HasSuffix(filename, ".ts") {
				filename += ".ts"
			}
			exist, err := gkaylee.CheckFileExist(filename)
			if err != nil {
				slog.Warn("检查文件是否存在失败", slog.Any("err", err))
				return agg
			} else if exist {
				return agg + 1
			}
			return agg
		}, 0)
		bar.SetCurrent(downloadedOfNum)

		// m3u8下载器
		downloader := core.NewDownloader(
			core.WithConcurrentMax(int(cfg.Concurrency)),
			core.WithDownloadRetriesMax(int(cfg.RetryOfNum)),
		)
		// 开始下载
		err = downloader.Download(ctx, m8.PlayList,
			downloadMediaEventBus.NewBus(tsSavePath, logger,
				downloadMediaEventBus.WithAllowedErrorOfNum(int(cfg.AllowErrorOfNum)),
				downloadMediaEventBus.WithResponsePayloadMinSizeOfKb(int(cfg.TsMinKB)),
				downloadMediaEventBus.WithPB(downloadMediaEventBus.ProgressBarFun(func() {
					bar.Increment()
				})),
			),
		)
		bar.Finish()
		if err != nil {
			if errors.Is(err, context.Canceled) {
				logger.Warn("下载被取消")
			} else {
				logger.Error("下载失败", "err", err)
			}
			return
		}

		// 修改m3u8文件名后缀为ok
		err = os.Rename(m3u8Filename, m3u8Filename+".ok")
		if err != nil {
			logger.Error(`修改M3U8文件名后缀为".ok"失败`, slog.Any("err", err))
			return
		}

		// 合并所有流
		logger.Info("TS合并中...")
		tsAllInOneFilename := fmt.Sprintf("%s.ts", tsSavePath)
		err = MergeAllTsFromM3U8(tsSavePath, tsAllInOneFilename, m8)
		if err != nil {
			logger.Error("TS文件合并失败", "err", err)
			return
		}

		// 创建Ffmpeg
		if len(cfg.FfmpegBinFilename) == 0 {
			success = true
			logger.Warn("配置文件未配置ffmpeg, 无法将合并成功的TS转为MP4, 如需要转MP4请在配置文件中配置ffmpeg.")
			return
		}
		ffmpegClient, err := ffmpeg.NewClient(cfg.FfmpegBinFilename)
		if err != nil {
			logger := logger.With(slog.Any("ffmpegFilename", cfg.FfmpegBinFilename))
			if errors.Is(err, ffmpeg.ErrFfmpegFileNotExist) {
				logger.Warn("配置的ffmpeg文件不存在, 无法将合并成功的TS转为MP4, 如需要转MP4请配置正确的ffmpeg路径.")
				return
			}
			logger.Error("创建ffmpeg客户端失败", "err", err)
			return
		}
		// TS 转为 Mp4 文件
		var outMP4Filename string
		outMP4Filename, _ = cmd.Flags().GetString("output")
		if len(outMP4Filename) == 0 {
			outMP4Filename = filepath.Join(mp4SaveDir, projectName+".mp4")
		}
		err = ffmpegClient.FormatConversion(ctx, tsAllInOneFilename, outMP4Filename)
		if err != nil {
			execErr := new(ffmpeg.ExecError)
			if errors.As(err, &execErr) {
				logger.Error("TS文件转MP4失败", "err", err, "ffmpeg echo", execErr.Echo)
			} else {
				logger.Error("TS文件转MP4失败", "err", err)
			}
			return
		}
		// 自动清理TS文件
		isClean, _ := cmd.Flags().GetBool("clean")
		if isClean {
			cleanCmd.Run(cmd, args)
		}
		success = true
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		panic(err)
	}
}

func init() {
	var configFilename, referer string
	cobra.OnInitialize(func() {
		// 初始化配置文件
		var err error
		config.Default, err = config.LoadConfig(configFilename)
		if err != nil {
			if len(configFilename) > 0 {
				slog.Error("无法加载配置文件", "err", err, "configFilename", configFilename)
			} else {
				slog.Error("无法加载配置文件", "err", err)
			}
		}
		// 初始化日志
		initLog()
		// 初始化HTTP客户端
		initHTTPClient(referer)
	})
	rootCmd.PersistentFlags().StringVar(&configFilename, "config", "", "config file (default is ./config.yaml)")
	rootCmd.Flags().BoolP("clean", "c", false, "当视频下载生成MP4后自动清理TS")
	rootCmd.Flags().StringP("output", "o", "", "当不提供保存目录时, 保存在工作目录下的mp4文件夹中")
	rootCmd.PersistentFlags().StringVarP(&referer, "referer", "r", "", "直接指定referer")
	rootCmd.Flags().StringP("user-agent", "u", "", "指定UserAgent")
	gkaylee.IgnoreErr0(viper.BindPFlag("userAgent", rootCmd.Flags().Lookup("user-agent")))
}

// initLog 初始化日志
func initLog() {
	const errKey = "err"
	handler := tint.NewHandler(global.StdErr, &tint.Options{
		AddSource:  true,
		Level:      slog.LevelInfo,
		TimeFormat: "01-02 15:04:05",
		ReplaceAttr: func(groups []string, attr slog.Attr) slog.Attr {
			if attr.Key == errKey {
				val := attr.Value.Any()
				err, ok := val.(error)
				if !ok {
					return attr
				}

				// 检查错误是否包含多个错误, 如果是, 修改其格式化方式
				me := new(multierror.Error)
				if errors.As(err, &me) {
					me.ErrorFormat = func(es []error) string {
						if len(es) == 1 {
							return fmt.Sprintf("1 error occurred: %q", es[0])
						}

						str := lo.Reduce(es, func(agg string, item error, index int) string {
							if index == 0 {
								return fmt.Sprintf("{ %q }", item)
							}
							return agg + fmt.Sprintf(",{ %q }", item)
						}, "")

						return fmt.Sprintf("%d errors occurred:[%s]", len(es), str)
					}
					err = me
				}

				// 断言最顶层的错误是否为 tracerr.Error 即可, 不需要 errors.As()
				// 否则栈信息与错误不匹配或错误不为最顶层的信息
				if te, ok := val.(tracerr.Error); ok {
					// 错误带跟踪栈格式化栈内容
					traces := lo.Reduce(te.StackTrace(), func(agg string, item tracerr.Frame, index int) string {
						data := fmt.Sprintf("%s:%d", item.Path, item.Line)
						if index == 0 {
							return data
						}
						return agg + " <- " + data
					}, "")
					// 将信息包装在分组中
					attr.Value = slog.GroupValue(
						tint.Err(err),
						slog.Any("traces", traces),
					)
					return attr
				} else {
				}
				return tint.Err(err)
			}
			return attr
		},
	})
	slog.SetDefault(slog.New(handler))
}

// initHTTPClient 初始化HTTP客户端
func initHTTPClient(referer string) {
	cfg := config.Default

	proxy := http.ProxyFromEnvironment
	if len(cfg.ProxyAddr) > 0 {
		u, err := tracerr.Wrap1(url.Parse(cfg.ProxyAddr))
		if err != nil {
			slog.Warn("解析代理地址出错", "err", err, "proxyAddr", cfg.ProxyAddr)
		} else {
			proxy = http.ProxyURL(u)
		}
	}
	var roundTrip http.RoundTripper

	roundTrip = &http.Transport{
		Proxy:               proxy,
		TLSHandshakeTimeout: 5 * time.Second,
		MaxIdleConns:        0, // 无限
		MaxIdleConnsPerHost: 100,
		MaxConnsPerHost:     0, // 无限
		IdleConnTimeout:     time.Minute * 2,
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}

	if len(referer) > 0 {
		roundTrip = func(roundTrip http.RoundTripper) http.RoundTripper {
			// 闭包保存一下roundTrip值的指向
			return roundTriper.WithCustom(func(req *http.Request) (*http.Response, error) {
				req.Header.Set("Referer", referer)
				return roundTrip.RoundTrip(req)
			})
		}(roundTrip)
	} else {
		if len(cfg.RefererConfigs) > 0 {
			refererMap := lo.SliceToMap(cfg.RefererConfigs, func(item config.RefererConfig) (string, string) {
				return item.Rule, item.Referer
			})
			roundTrip = roundTriper.NewWithHTTPReferer(roundTrip, refererMap)
		}
	}

	if len(cfg.UserAgent) > 0 {
		roundTrip = roundTriper.NewWithUserAgent(roundTrip, cfg.UserAgent)
	}

	roundTrip = retryTransport.New(roundTrip, gretry.DefaultRetryMax)

	http.DefaultClient = &http.Client{
		Transport: roundTrip,
		Timeout:   time.Duration(cfg.DownloadTimeoutSecond) * time.Second,
	}
}

func newProgressBar(max int, prefix string) *pb.ProgressBar {
	const pbTemplate pb.ProgressBarTemplate = `{{with string . "prefix"}}{{blue .}} {{end}}{{counters . }}: {{ bar . "<" "-" (cycle . "↖" "↑" "↗" "→" "↘" "↓" "↙" "←" ) "." ">"}} {{speed . | rndcolor }} {{percent .}} {{string . "my_green_string" | green}} {{string . "my_blue_string" | blue}}`

	bar := pbTemplate.Start(max)
	bar.SetWriter(global.StdErr)
	bar.Set("prefix", prefix)
	bar.SetMaxWidth(100)
	bar.SetRefreshRate(time.Second)
	return bar
}

func MergeAllTsFromM3U8(mediaSaveFilePath, outFilename string, m8 *m3u8.M3U8) error {
	if len(m8.PlayList) == 0 {
		return tracerr.New("m8.PlayList is empty")
	}

	// 创建保存文件
	file, err := os.Create(outFilename)
	if err != nil {
		return tracerr.Wrap(err)
	}
	defer file.Close()

	// 500MB的写入缓存
	writer := bufio.NewWriterSize(file, 500*1024*1024)
	defer func() {
		// 将缓存写入磁盘
		err2 := writer.Flush()
		if err2 != nil && err == nil {
			err = err2
		}
	}()

	// 遍历M3U8列表并读入TS数据
	fs := os.DirFS(mediaSaveFilePath)
	for _, media := range m8.PlayList {
		name := path.Base(media.Addr)
		if !strings.HasSuffix(name, ".ts") {
			name += ".ts"
		}
		tsFile, err := fs.Open(name)
		if err != nil {
			continue
		}
		gkaylee.IgnoreErr(writer.ReadFrom(tsFile))
		gkaylee.IgnoreErr0(tsFile.Close())
	}
	return nil
}
