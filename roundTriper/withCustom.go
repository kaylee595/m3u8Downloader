package roundTriper

import (
	"net/http"
)

type WithCustom func(*http.Request) (*http.Response, error)

func (w WithCustom) RoundTrip(request *http.Request) (*http.Response, error) {
	return w(request)
}
