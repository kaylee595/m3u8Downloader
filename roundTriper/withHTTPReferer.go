package roundTriper

import (
	"cmp"
	"net/http"
	"regexp"
)

type WithHTTPReferer struct {
	tripper http.RoundTripper
	refMap  map[string]string
}

func (r *WithHTTPReferer) RoundTrip(request *http.Request) (*http.Response, error) {
	for rule, referer := range r.refMap {
		ok, err := regexp.MatchString(rule, request.Host)
		if err == nil && ok {
			request.Header.Set("Referer", referer)
			break
		}
	}
	return r.tripper.RoundTrip(request)
}

func NewWithHTTPReferer(tripper http.RoundTripper, ref map[string]string) *WithHTTPReferer {
	return &WithHTTPReferer{
		tripper: cmp.Or(tripper, http.DefaultTransport),
		refMap:  ref,
	}
}
