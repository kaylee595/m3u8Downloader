package roundTriper

import (
	"cmp"
	"net/http"
)

type WithUserAgent struct {
	tripper http.RoundTripper
	ua      string
}

func (r *WithUserAgent) RoundTrip(request *http.Request) (*http.Response, error) {
	if len(r.ua) > 0 {
		request.Header.Set("User-Agent", r.ua)
	}
	return r.tripper.RoundTrip(request)
}

func NewWithUserAgent(tripper http.RoundTripper, ua string) *WithUserAgent {
	return &WithUserAgent{
		tripper: cmp.Or(tripper, http.DefaultTransport),
		ua:      ua,
	}
}
