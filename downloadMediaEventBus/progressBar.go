package downloadMediaEventBus

type ProgressBar interface {
	Increment()
}

type ProgressBarFun func()

func (p ProgressBarFun) Increment() {
	p()
}
