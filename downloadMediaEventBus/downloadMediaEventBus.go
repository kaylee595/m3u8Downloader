package downloadMediaEventBus

import (
	"cmp"
	"context"
	"errors"
	"gitee.com/kaylee595/gkaylee"
	"github.com/kaylee595/tracerr"
	"log/slog"
	"m3u8Downloader/core"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
	"sync"
	"sync/atomic"
)

type Bus struct {
	dir                        string
	pb                         ProgressBar
	allowedErrorOfNum          int64
	responsePayloadMinSizeOfKb int
	logger                     *slog.Logger

	onceMakeDir       sync.Once
	currentErrorOfNum int64
}

func NewBus(dir string, logger *slog.Logger, options ...Option) *Bus {
	ret := &Bus{
		dir:    dir,
		pb:     ProgressBarFun(func() {}),
		logger: cmp.Or(logger, slog.Default()),
	}
	for _, option := range options {
		option(ret)
	}
	return ret
}

func (b *Bus) BeforeDownloadMedia(ctx *core.Context) error {
	u, err := url.Parse(ctx.Media.Addr)
	if err != nil {
		return tracerr.Wrap(err)
	}
	name := path.Base(u.Path)
	if !strings.HasSuffix(name, ".ts") {
		name += ".ts"
	}
	exist, err := b.isFileExist(name)
	if err != nil {
		return tracerr.Wrap(err)
	}
	if exist == true {
		return tracerr.Wrap(ErrDownloaded)
	}
	return nil
}

func (b *Bus) AfterDownloadMedia(ctx *core.Context) error {
	logger := b.logger.With(slog.String("addr", ctx.Media.Addr))

	select {
	case <-ctx.Done():
		err := context.Cause(ctx)
		if errors.Is(err, ErrDownloaded) || errors.Is(err, context.Canceled) {
			return nil
		}
		defer b.pb.Increment()
		return b.occurredError(logger, tracerr.Wrap(err))
	default:
	}
	defer b.pb.Increment()

	err := b.isResponsePayloadCompliance(ctx.ResponsePayload)
	if err != nil {
		return b.occurredError(logger, tracerr.Wrap(err))
	}

	// 拼接保存路径, 这里由于前面能够成功下载到资源, 所以url.Parse肯定是成功的, 直接忽略错误不处理
	u := gkaylee.IgnoreErr(url.Parse(ctx.Media.Addr))
	name := path.Base(u.Path)
	if !strings.HasSuffix(name, ".ts") {
		name += ".ts"
	}
	// 保存文件
	err = b.saveResponsePayload(name, ctx.ResponsePayload)
	if err != nil {
		return b.occurredError(logger, tracerr.Wrap(err))
	}
	return nil
}

func (b *Bus) isFileExist(name string) (bool, error) {
	return tracerr.Wrap1(gkaylee.CheckFileExist(filepath.Join(b.dir, name)))
}

func (b *Bus) isResponsePayloadCompliance(payload []byte) error {
	if len(payload) <= b.responsePayloadMinSizeOfKb*1024 {
		return tracerr.Errorf("%w, 预期: %d(kb), 实际: %d(kb)", ErrPayloadSizeNotMatch, b.responsePayloadMinSizeOfKb, len(payload)/1024)
	}
	return nil
}

func (b *Bus) saveResponsePayload(name string, payload []byte) error {
	// 在保存文件前创建一下文件夹, 使用once保证文件夹已经创建
	b.onceMakeDir.Do(func() {
		os.MkdirAll(b.dir, 0600)
	})

	name = filepath.Join(b.dir, name)
	err := os.WriteFile(name, payload, 0600)
	if err != nil {
		_ = os.Remove(name)
		return tracerr.Wrap(err)
	}
	return nil
}

func (b *Bus) occurredError(logger *slog.Logger, err error) error {
	occurredErrorOfNum := atomic.AddInt64(&b.currentErrorOfNum, 1)
	logger.Warn("下载失败", "err", err, "errorCount", occurredErrorOfNum)
	if occurredErrorOfNum >= b.allowedErrorOfNum {
		return tracerr.Errorf("%w, 预期: %d, 实际: %d", ErrOccurredErrorTooMuch, b.allowedErrorOfNum, occurredErrorOfNum)
	}
	return nil
}

var ErrPayloadSizeNotMatch = errors.New("载体大小不匹配")
var ErrDownloaded = errors.New("已下载")
var ErrOccurredErrorTooMuch = errors.New("出现错误太多")

type Option func(*Bus)

func WithAllowedErrorOfNum(val int) Option {
	return func(b *Bus) {
		b.allowedErrorOfNum = int64(val)
	}
}

func WithResponsePayloadMinSizeOfKb(val int) Option {
	return func(b *Bus) {
		b.responsePayloadMinSizeOfKb = val
	}
}

func WithPB(pb ProgressBar) Option {
	if pb == nil {
		pb = ProgressBarFun(func() {})
	}
	return func(b *Bus) {
		b.pb = pb
	}
}
